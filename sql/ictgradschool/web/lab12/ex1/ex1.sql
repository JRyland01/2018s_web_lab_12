-- Answers to exercise 1 questions
SELECT dept FROM unidb_courses;
SELECT semester FROM unidb_attend;
SELECT DISTINCT a.num FROM unidb_courses AS s, unidb_attend AS a WHERE s.num = a.num;
SELECT fName,lName,country FROM unidb_students ORDER BY fname;
SELECT fNAme, lName, mentor FROM unidb_students ORDER BY mentor;
SELECT * FROM unidb_lecturers ORDER BY office;
SELECT * FROM unidb_lecturers WHERE staff_no>500;
SELECT * FROM unidb_students WHERE id>1668 AND id<1824;
SELECT * FROM unidb_students WHERE country = 'NZ' OR country = 'AU' OR country = 'US';
SELECT * FROM unidb_lecturers WHERE office LIKE 'G%';
SELECT * FROM unidb_courses WHERE NOT dept =  'comp';
SELECT * FROM unidb_students WHERE country ='FR' OR country = 'MX';