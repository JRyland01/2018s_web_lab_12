-- Answers to exercise 2 questions
SELECT fname, lName FROM unidb_students AS s, unidb_attend AS a WHERE s.id = a.id AND a.dept = 'comp' AND a.num = 219;

SELECT fname, lName FROM unidb_students AS s, unidb_courses AS a WHERE s.id = a.rep_id AND NOT s.country ='NZ';

SELECT office FROM unidb_lecturers AS s, unidb_courses AS a WHERE s.staff_no = a.coord_no AND a.dept = 'comp'AND a.num='219';

SELECT DISTINCT s.fname,s.lName FROM unidb_students AS s, unidb_lecturers AS a, unidb_teach AS b, unidb_attend AS c
WHERE a.fname = 'Te Taka'
      AND b.staff_no = a.staff_no
      AND s.id = c.id
      AND c.dept = b.dept
      AND c.num = b.num;

SELECT a.fName, a.lName, b.fname, b.lname FROM unidb_students AS a, unidb_students AS b WHERE b.id = a.mentor;

SELECT a.fname, a.lname FROM unidb_lecturers AS a
WHERE a.office LIKE 'G%'
UNION
SELECT b.fname, b.lname FROM unidb_students AS b WHERE b.country = 'NZ';

SELECT a.fname, a.lname FROM unidb_lecturers AS a, unidb_courses AS c
WHERE a.staff_no = c.coord_no AND c.dept = 'comp' AND c.num = 219
UNION
SELECT b.fname, b.lname FROM unidb_students AS b, unidb_courses AS d
WHERE b.id = d.rep_id AND d.dept = 'comp' AND d.num = 219;