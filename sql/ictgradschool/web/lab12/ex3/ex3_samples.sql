DROP TABLE IF EXISTS league;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS player;

CREATE TABLE league (
  leagueId   INT         NOT NULL,
  leagueName VARCHAR(50) NOT NULL,
  PRIMARY KEY (leagueId)
);
CREATE TABLE team (
  teamId   INT          NOT NULL,
  teamName VARCHAR(100),
  homeCity VARCHAR(100),
  captain  VARCHAR(100) NOT NULL,
  points   INT          NOT NULL,
  leagueId INT NOT NULL,
  PRIMARY KEY (teamId),
  FOREIGN KEY (leagueId) REFERENCES league (leagueId)
);
CREATE TABLE player(
  playerId INT NOT NULL,
  teamId INT NOT NULL,
  age INT NOT NULL,
  name VARCHAR(100) NOT NULL,
  NATIONAL VARCHAR(100),
  PRIMARY KEY (playerId, teamId),
  FOREIGN KEY (teamId) REFERENCES team (teamId))
;

INSERT INTO league (leagueId, leagueName) VALUES
  (1,'Ligma'),
  (2,'Premiership');

INSERT INTO team (teamId, teamName, homeCity, captain, points, leagueId) VALUES
  (99,'Ligma United01','Ligma Town','Lord Ligma',100,1),
  (98,'Ligma United02','Auckland','Ronaldo',13,2),
  (97,'Ligma United03','Ligma Town','idamondoe',110,1),
  (96,'Ligma United04','Diamondville','Trump',7,1),
  (95,'Ligma United05','Ligma Town','Lord',54,2),
  (94,'Ligma United06','Ligma Village','Logma',100,1),
  (93,'Ligma United07','Ligma Town','La',34,2),
  (92,'Ligma United08','woooo','Lord Josja',40,1),
  (91,'Ligma United09','Ligma Town','Lgma',143,2),
  (90,'Ligma United10','Ligma City','Messi',23,1);

INSERT INTO player (playerId, teamId, age, name, NATIONAL) VALUES
  (5,99,43,'Bob', 'NZ'),
  (5,90,43,'Messi', 'FD'),
  (5,96,43,'Trump', 'NZ'),
  (5,97,43,'idamondoe', 'AT'),
  (5,98,43,'Ronaldo', 'AS');